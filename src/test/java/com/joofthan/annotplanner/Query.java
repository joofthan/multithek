package com.joofthan.annotplanner;

public @interface Query {
    String value();
    String[] parameters() default {};
}
