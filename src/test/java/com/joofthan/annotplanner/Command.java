package com.joofthan.annotplanner;

public @interface Command {
    String value();
    String[] parameters() default {};
}
