package com.joofthan.lib.template;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TemplateTest {

    Template template = new QuteTemplateImpl();

    @Test
    void each(){
        String result = template.render("""
                {#each myVal}
                <a>{it.name}</a>
                {/each}
                """, "myVal", List.of(new MyEntity("Joe"),new MyEntity("Don")));
        assertEquals("""
                <a>Joe</a>
                <a>Don</a>
                """,result);
    }

    record MyEntity(String name){}
}