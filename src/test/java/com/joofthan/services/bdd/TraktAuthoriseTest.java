package com.joofthan.services.bdd;


import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class TraktAuthoriseTest {

    /*
    @Test
    void should_return_authoriseurl_when_user_not_authorised(){
        given().trakt_staging_configuration("https://trakt.tv/","clientId", "secret", 1234);

        when().redirect_url_was_generated("http://localhost:8080/trakt/success");

        String url = "https://trakt.tv/" + "/oauth/authorize?response_type=code&client_id=" + "clientId" + "&redirect_uri=" + URLEncoder.encode("http://localhost:8080/trakt/success", StandardCharsets.UTF_8) + "&state=" + 1234;
        then().is_not_authorised().and().authorise_url_is_(url);
    }

    @Test
    void should_be_authorised_when_accesstoken_is_valid(){
        given().trakt_staging_configuration("https://trakt.tv/","clientId", "secret", 1234)
                .and().internet_connection_on_post(
                        (url, body) -> url.equals("https://trakt.tv/oauth/token")?"finalAccessToken":null
                );

        when().redirect_url_was_generated("http://localhost:8080/trakt/success")
                .and().fetch_finalAccessToken_with_responded_tempToken("tempAccessToken", 1234);

        then().token_is("finalAccessToken")
                .and().is_authorised();
    }

    @Test
    void should_send_body_when_authorise_trakt(){
        given().trakt_staging_configuration("https://trakt.tv/","clientId", "secret", 1234)
                .and().internet_connection_on_post(
                        (url, body) -> url.equals("https://trakt.tv/oauth/token")?"finalAccessToken from: "+body:null
                );

        when().redirect_url_was_generated("http://localhost:8080/trakt/success")
                .and().user_was_redirected()
                .then().fetch_finalAccessToken_with_responded_tempToken("tempToken", 1234);

        then().token_is("finalAccessToken from: {\"code\":\"tempToken\",\"client_id\":\"clientId\",\"client_secret\":\"secret\",\"redirect_uri\":\"http://localhost:8080/trakt/success\",\"grant_type\":\"authorization_code\"}")
                .and().is_authorised();
    }


     */





    /*
    App app;
    private TraktConfig conf;

    @BeforeEach
    void init(){


        app = AppFactory.createTestConfig(conf);
    }

    @Test
    void should_returnAuthoriseUrl_when_userNotAuthorised(){
        String state = String.valueOf(RANDOM_NUMBER);
        String backTo = "http://localhost:8080/trakt/success";
        String url = conf.baseAuthoriseUrl()+"/oauth/authorize?response_type=code&client_id=" +conf.clientId() + "&redirect_uri="+ URLEncoder.encode(backTo, StandardCharsets.UTF_8)+"&state="+state;
        var res = app.run(LoadTraktStatus.class, new LoadTraktStatus.TraktLoadStatusRequest(state, backTo));
        assertFalse(res.isAuthorised());
        assertEquals(url, res.authoriseUrl());
    }

    @Test
    void should_beAuthorised_when_accesstokenIsValid(){
        String state = String.valueOf(RANDOM_NUMBER);
        String code = "todo";
        app.run(LoadTraktStatus.class, new LoadTraktStatus.TraktLoadStatusRequest(state, "http://backTo"));
        var res = app.run(AuthoriseTrakt.class, new AuthoriseTrakt.TraktAuthoriseRequest(state, code));
        assertEquals("myToken", conf.tokenStorage().findToken().get());
        assertTrue(res.isAuthorised());
    }

     */
}
