package com.joofthan.apps.multithek.backend.home.boundary;

import com.joofthan.BaseTest;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class SettingsEndpointTest extends BaseTest {

    @Inject
    SettingsEndpoint settingsEndpoint;

    @Test
    void addLoadDelete(){
        assertEquals(0,settingsEndpoint.listProviders().size());
        settingsEndpoint.add("example");
        assertEquals(1,settingsEndpoint.listProviders().size());
        settingsEndpoint.delete(settingsEndpoint.listProviders().get(0).id);
        assertEquals(0,settingsEndpoint.listProviders().size());
    }

}