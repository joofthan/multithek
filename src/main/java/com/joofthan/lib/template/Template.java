package com.joofthan.lib.template;

import java.util.Map;

public interface Template {
    String render(String template);
    String render(String template, String key, Object obj);

    String render(String template, Map<String, Object> map);
}
