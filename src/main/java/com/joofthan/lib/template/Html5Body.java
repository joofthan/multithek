package com.joofthan.lib.template;

public class Html5Body {
    String part1 = """
            <!DOCTYPE html>
            <html>
            <head>
              <title>""";

        String part2 = """
              </title>
              <link rel="stylesheet" href="/multithek/css/style.css" />
              <link rel="stylesheet" href="/shared/apputil/autostyle.css" />
              <script src="/webjars/htmx.org/dist/htmx.min.js"></script>
            </head>
            <body style="margin: 0;padding: 0;">
            """;
    private String title;
    private String content;

    public Html5Body(String title, String content){
        this.title = title;
        this.content = content;
    }

    @Override
    public String toString() {
        return part1 + title + part2 + content + "</body>\n</html>";
    }
}
