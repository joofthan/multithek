package com.joofthan.lib;

public class Log {
    public static void info(String message){
        System.out.println(message);
    }

    public static void info(String message, Exception e) {
        e.printStackTrace(System.out);
        System.out.println(message);
    }

    public static void slowQuery(Class clazz) {
        info("Slow query: "+clazz);
    }

    public static void memoryLeak(Class aClass) {
        info("Possible MemoryLeak: "+aClass);
    }
}
