package com.joofthan.apps.multithek;

import com.joofthan.lib.template.Template;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class MultithekExceptionHandler implements ExceptionMapper<MultithekException> {
    @Inject
    Template template;

    @Override
    public Response toResponse(MultithekException exception) {
        //language="html"
        return Response.status(200).header("HX-Retarget","#messages").entity(template.render("""
                <script>
                alert('{msg}');
                </script>
                
                """, "msg", exception.getMessage())).build();
    }
}

