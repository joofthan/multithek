package com.joofthan.apps.multithek.backend.providers.control;

public class Image {
    private String imageUrl;

    public Image(String url){
        imageUrl = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public static Image NONE=new Image("/multithek/img/livetv.png");
}
