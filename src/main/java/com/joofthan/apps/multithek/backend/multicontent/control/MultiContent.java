package com.joofthan.apps.multithek.backend.multicontent.control;

import com.joofthan.apps.multithek.backend.providers.control.Content;

import java.util.List;

public class MultiContent extends Content {
    private final List<ContentProviderId> providerIds;

    private MultiContent(Content content, ContentProviderId... providerId) {
        super(content);
        this.providerIds = List.of(providerId);
    }

    public static MultiContent of(Content content, ContentProviderId... providerId) {
        return new MultiContent(content, providerId);
    }

    public List<ContentProviderId> getProviderIds() {
        return providerIds;
    }

    public static record ContentProviderId(Long value) {
        public static ContentProviderId of(Long id) {
            return new ContentProviderId(id);
        }
    }
}
