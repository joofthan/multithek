package com.joofthan.apps.multithek.backend.providers.control;

public record ContentAction(
        ActionType type,
        String url
) {
    public static ContentAction url(String url) {
        return new ContentAction(ActionType.URL, url);
    }

    public enum ActionType {
        GROUP, SINGLE, URL, VIDEO, AUDIO, HTML
    }
}
