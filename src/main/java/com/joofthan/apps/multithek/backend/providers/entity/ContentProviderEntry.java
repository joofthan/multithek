package com.joofthan.apps.multithek.backend.providers.entity;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Deprecated
//@Entity
@Table(name = "CONTENTPROVIDER")
class ContentProviderEntry {
    @Id
    @GeneratedValue
    private Long id;
    private Long userId;
    private String url;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CONTENTPROVIDER_ID")
    private List<ContentProviderKeyValueDetail> details;

    public ContentProviderEntry(){}
    public ContentProviderEntry(String url) {
        this.url = url;
        this.details = new ArrayList<>();
    }

    public ContentProviderEntry(String url, List<ContentProviderKeyValueDetail> details) {
        this.url = url;
        this.details = details;
    }

    public List<ContentProviderKeyValueDetail> getDetails(){
        return Collections.unmodifiableList(details);
    }

    public Optional<ContentProviderKeyValueDetail> getDetail(String key){
        List<ContentProviderKeyValueDetail> list = details.stream().filter(d -> key.equals(d.getMyKey())).collect(Collectors.toList());
        if(list.isEmpty()){
            return Optional.empty();
        }else if(list.size() == 1){
            return Optional.of(list.get(0));
        }else{
            throw new IllegalStateException("More than one Key \"" +key+"\" in "+ ContentProviderEntry.class.getSimpleName()+" with id \""+this.getId()+"\"");
        }
    }

    public void setDetail(String key, String value){
        Optional<ContentProviderKeyValueDetail> detail = getDetail(key);
        if(detail.isPresent()){
            detail.get().setMyValue(value);
        }else{
            details.add(new ContentProviderKeyValueDetail(key, value));
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Deprecated
    public void setDetails(List<ContentProviderKeyValueDetail> details) {
        this.details = details;
    }
}