package com.joofthan.apps.multithek.backend.providers.control;


import java.time.LocalDateTime;

public class Content {
    private UniversalContentId id;
    private ContentType type;
    private String title;
    private String description;
    private String imageUrl;
    private LocalDateTime lastEdit;
    private int progress;
    private ContentAction contentAction;


    @Deprecated
    public Content(UniversalContentId id, ContentType type, String title, String description, String imageUrl, LocalDateTime lastEdit, int progress, ContentAction contentAction) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.lastEdit = lastEdit;
        this.progress = progress;
        this.contentAction = contentAction;
    }

    public Content(UniversalContentId id, ContentType type, String title, String description, Image image, LocalDateTime lastEdit, int progress, ContentAction contentAction) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.description = description;
        this.imageUrl = image.getImageUrl();
        this.lastEdit = lastEdit;
        this.progress = progress;
        this.contentAction = contentAction;
    }

    public Content(Content other){
        this.id = other.id;
        this.type = other.type;
        this.title = other.title;
        this.description = other.description;
        this.imageUrl = other.imageUrl;
        this.lastEdit = other.lastEdit;
        this.progress = other.progress;
        this.contentAction = other.contentAction;
    }

    public enum ContentType {
        LIVE,
        SERIES,
        MOVIE,
        VIDEO,
        MUSIC,
        PODCAST,
        AUDIOBOOK,
        BOOK,
        NEWS
    }


    public UniversalContentId getId() {
        return id;
    }

    public ContentType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public LocalDateTime getLastEdit() {
        return lastEdit;
    }

    public int getProgress() {
        return progress;
    }

    public ContentAction getContentAction() {
        return contentAction;
    }
}
