package com.joofthan.apps.multithek.backend.home.control;

import com.joofthan.apps.multithek.backend.home.entity.UiCover;
import com.joofthan.apps.multithek.backend.multicontent.control.MultiContentRepository;
import com.joofthan.apps.multithek.backend.providers.control.Content;
import com.joofthan.lib.Injectable;
import jakarta.inject.Inject;

import java.util.List;
import java.util.stream.Collectors;

@Injectable
public class FilterRepository {

    @Inject
    MultiContentRepository providers;


    public List<UiCover> findContinue(Content.ContentType type){
        return providers.getContinue().stream()
                .filter(e->e.getType() == type)
                .map(UiCover::of)
                .collect(Collectors.toList());
    }

    public List<UiCover> findRecommendation(Content.ContentType type){
        return providers.getRecommendations().stream()
                .filter(e->e.getType() == type)
                .map(UiCover::of)
                .collect(Collectors.toList());
    }
}
