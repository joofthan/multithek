package com.joofthan.apps.multithek.backend.providers.control;

/**
 *
 * This Content ID is Content specific. 2 Providers must provide the same ID.
 * Maybe an UniversalContentIdService is needed for global ID retrieval.
 *
 */
public record UniversalContentId(Long value) {
    public static UniversalContentId of(Long l) {
        return new UniversalContentId(l);
    }

}
