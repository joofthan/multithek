package com.joofthan.apps.multithek.backend.home.entity;

import com.joofthan.apps.multithek.backend.multicontent.control.MultiContent;

public record UiCover(
        String image,
        String placeholder,
        String coverType,
        String url,
        Long id
) {
    public static UiCover of(MultiContent c) {
        return new UiCover(
                c.getImageUrl(),
                c.getTitle(),
                c.getType().name(),
                c.getContentAction().url(),
                c.getId().value()
        );
    }

    public UiCover getCover() {
        return null;
    }

    public String getImage(){
        return image;
    }

    public String getPlaceholder(){
        return placeholder;
    }

    public String getUrl(){
        return url;
    }

    public Long getId(){
        return id;
    }
}


