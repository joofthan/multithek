package com.joofthan.apps.multithek.backend.home.control;

@Deprecated
enum UiContentTyp {
    COVER,
    VIDEO_PREVIEW,
    TEXT,
    AUDIO
}
