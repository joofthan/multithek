package com.joofthan.apps.multithek.backend.home.control;

import com.joofthan.apps.multithek.backend.home.entity.UiCover;
import com.joofthan.apps.multithek.backend.multicontent.control.MultiContentRepository;
import com.joofthan.lib.InjectableProxy;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@InjectableProxy
public class HomeService implements Serializable {

    @Inject
    MultiContentRepository homeRepository;

    private List<UiCover> continueRow;

    @PostConstruct
    public void init(){
        continueRow = homeRepository.getContinue()
                .stream()
                .map(UiCover::of)
                .collect(Collectors.toList());
    }

    public List<UiCover> homeContinue(){
        return continueRow;
    }
}
