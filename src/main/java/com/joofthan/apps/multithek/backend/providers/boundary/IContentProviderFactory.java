package com.joofthan.apps.multithek.backend.providers.boundary;

import com.joofthan.apps.multithek.backend.providers.control.IContentProvider;
import com.joofthan.apps.multithek.backend.providers.entity.ProviderUrlDB;
import com.joofthan.lib.Injectable;
import lombok.RequiredArgsConstructor;

import jakarta.enterprise.inject.Instance;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Injectable
@RequiredArgsConstructor
public class IContentProviderFactory {
    final Instance<IContentProvider> cdiProviders;
    final ProviderUrlRepository providerUrlRepository;

    public List<IContentProvider> getConfiguredProviders() {
        var list = new ArrayList<IContentProvider>();
        List<ProviderUrlDB> urls= providerUrlRepository.findAll();
        for (ProviderUrlDB providerUrl:urls) {
            var provider = findCdiProvider(providerUrl.getUrl());
            if(provider.isPresent()){
                list.add(provider.get());
            }else {
                list.add(createFromUrl(providerUrl.getUrl()));
            }
        }

        return list;
    }

    private IContentProvider createFromUrl(String url) {
        throw new UnsupportedOperationException("UrlContentProvider not implemented yet. Remove from Settings: "+url);
    }

    private Optional<IContentProvider> findCdiProvider(String name){
        return cdiProviders.stream().filter(p->p.getInfo().name().equals(name)).findFirst();
    }

    public List<String> getDemoUrls() {
        return cdiProviders.stream().map(e->e.getInfo().name()).collect(Collectors.toList());
    }
}
