package com.joofthan.apps.multithek.backend.home.boundary;

import com.joofthan.apps.multithek.MultithekApi;
import com.joofthan.apps.multithek.backend.home.control.FilterRepository;
import com.joofthan.apps.multithek.backend.home.entity.UiCover;
import com.joofthan.apps.multithek.backend.providers.control.Content;
import com.joofthan.lib.TransactionalBoundary;
import com.joofthan.lib.template.Template;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.List;
import java.util.Map;


@TransactionalBoundary
@Path(MultithekApi.PATH+"/api/live")
@Produces(MediaType.APPLICATION_JSON)
public class LiveEndpoint {

    @Inject
    FilterRepository contentService;

    @Inject
    Template template;

    @Path("continue")
    @GET
    public List<UiCover> getActive(){
        return contentService.findContinue(Content.ContentType.LIVE);
    }

    @Path("recommend")
    @GET
    public List<UiCover> getRecommend(){
        return contentService.findRecommendation(Content.ContentType.LIVE);
    }

    @GET
    public String html(){
        return template.render("""
                 <div style="text-align: center;font-size: 1.5em;">
                            {#if active.size() != 0}
                                <h3>Continue</h3>
                                {#each active}
                                    <a class="link" href="{it.url}" target="_blank">{it.placeholder}</a><br/>
                                {/each}
                            {/if}
                            <h3>Live TV:</h3>
                            {#each recommend}
                                <a class="link" href="{it.url}" target="_blank">{it.placeholder}</a><br/>
                            {/each}
                                
                        </div>
                """, Map.of("active",getActive(),"recommend",getRecommend()));
    }
}
