package com.joofthan.apps.multithek.backend.providers.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ProviderConfigEntryDB extends PanacheEntity {
    @NotNull
    private Long providerUrlId;
    private String myKey;
    private String myValue;
}
