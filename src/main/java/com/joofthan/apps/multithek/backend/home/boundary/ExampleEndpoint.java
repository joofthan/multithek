package com.joofthan.apps.multithek.backend.home.boundary;

import com.joofthan.apps.multithek.MultithekApi;
import com.joofthan.apps.multithek.backend.home.control.HomeService;
import com.joofthan.apps.multithek.backend.home.entity.UiCover;
import com.joofthan.lib.TransactionalBoundary;
import com.joofthan.lib.template.Template;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

@TransactionalBoundary
@Path(MultithekApi.PATH+"/api/home")
public class ExampleEndpoint {

    @Inject
    HomeService home;

    @Inject
    Template template;

    @GET
    public List<UiCover> homeContent(){
        return home.homeContinue();
    }

    @GET
    @Path("test2")
    @Produces(MediaType.TEXT_HTML)
    public String thymeleaf(){
        return template.render("""
                 <a th:each="c : ${covers}" th:href="${c.url}" class="cover">
                    <img th:src="${c.image}" th:alt="${c.placeholder}"/>
                 </a>
                ""","covers",home.homeContinue());
    }

    @GET
    @Path("my")
    @Produces(MediaType.TEXT_HTML)
    public String my(){
        return template.render("""
                 <a foreach="covers as c" href="{c.url}" class="cover">
                    <img src="{c.image}" alt="{c.placeholder}"/>
                 </a>
                ""","covers",home.homeContinue());
    }

    public String full(){
        return template.render("""
                 <a foreach="covers as c" href="{c.url}" class="cover">
                    <img if="covers.size() >= 5" src="{c.image}" alt="{c.placeholder}"/>
                    <img else if="xxx" src="{c.image}" alt="{c.placeholder}"/>
                    <img else src="{c.image}" alt="{c.placeholder}"/>
                 </a>
                ""","covers",home.homeContinue());
    }
}
