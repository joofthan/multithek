package com.joofthan.apps.multithek.backend.home.entity;

import jakarta.ws.rs.Path;

public class Link {
    public static final String BASE_URL = "/multithek";
    public final String url;

    private Link(String url){
        this.url = url;
    }

    public String url(){
        return url;
    }


    public static Link to(String link) {
        return new Link(BASE_URL + link);
    }

    public static <T> Link to(Class<T> clazz) {
        if(clazz == null){
            throw new RuntimeException("Link to null is not allowed.");
        }
        return new Link(clazz.getAnnotation(Path.class).value());
    }
    public static <T> Link to(Class<T> clazz, String add) {
        if(clazz == null){
            throw new RuntimeException("Link to null is not allowed.");
        }
        return new Link(clazz.getAnnotation(Path.class).value() + add);
    }

    @Override
    public String toString() {
        return url;
    }
}
