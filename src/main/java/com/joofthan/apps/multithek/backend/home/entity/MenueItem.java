package com.joofthan.apps.multithek.backend.home.entity;

public record MenueItem(
        String text,
        Link target,
        boolean active
){
    public MenueItem(String text, Link link) {
        this(text,link,false);
    }

    public boolean isActive(){
        return active;
    }
}
