package com.joofthan.apps.multithek.backend.providers.control;

import java.math.BigDecimal;

public record SubscriptionPrice(int durationInMonth, BigDecimal priceSum) {
    public static SubscriptionPrice month(double price) {
        return new SubscriptionPrice(1, BigDecimal.valueOf(price));
    }

    public static SubscriptionPrice none() {
        return month(0.0);
    }
}
