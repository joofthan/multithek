package com.joofthan.apps.multithek.backend.providers.control;

import java.util.List;

public record ContentProviderInfo(
        boolean isSubscribed,
        SubscriptionPrice subscriptionPrice,
        String  name ,
        String  description ,
        List<AuthorizeAction> actions ,
        List<String> requiredValues
) {}
