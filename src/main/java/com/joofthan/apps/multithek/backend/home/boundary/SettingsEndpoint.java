package com.joofthan.apps.multithek.backend.home.boundary;

import com.joofthan.apps.multithek.backend.providers.boundary.ProviderUrlRepository;
import com.joofthan.apps.multithek.backend.providers.entity.ProviderUrlDB;
import com.joofthan.lib.TransactionalBoundary;
import com.joofthan.apps.multithek.MultithekApi;
import lombok.RequiredArgsConstructor;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;

@TransactionalBoundary
@RequiredArgsConstructor
@Path(MultithekApi.PATH+"settings/api")
public class SettingsEndpoint {

    final ProviderUrlRepository providerUrlRepository;

    @GET
    public List<ProviderUrlDB> listProviders(){
        return providerUrlRepository.findAll();
    }

    public void add(String url) {
        if(url == null || url.isBlank()) throw new IllegalArgumentException();
        ProviderUrlDB providerUrl = new ProviderUrlDB(url);
        providerUrlRepository.persist(providerUrl);
    }

    public void delete(Long id) {
        providerUrlRepository.deleteById(id);
    }
}
