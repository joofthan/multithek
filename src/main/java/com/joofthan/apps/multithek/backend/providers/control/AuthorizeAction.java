/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.joofthan.apps.multithek.backend.providers.control;

/**
 *
 * @author Jonathan
 */
public record AuthorizeAction(String name, String url) {
    
}
