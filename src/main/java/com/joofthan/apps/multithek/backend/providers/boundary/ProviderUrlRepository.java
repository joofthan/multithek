package com.joofthan.apps.multithek.backend.providers.boundary;

import com.joofthan.apps.multithek.backend.providers.entity.ProviderUrlDB;
import lombok.RequiredArgsConstructor;

import jakarta.enterprise.context.RequestScoped;
import jakarta.persistence.EntityManager;
import java.util.List;

@RequestScoped
@RequiredArgsConstructor
public class ProviderUrlRepository {
    final EntityManager em;
    final IContentProviderFactory contentProviderFactory;

    public void persist(ProviderUrlDB providerUrl) {
        em.merge(providerUrl);
    }

    public void deleteById(Long id) {
        var e = em.find(ProviderUrlDB.class, id);
        em.remove(e);
    }

    public List<ProviderUrlDB> findAll() {
        List resultList = em.createQuery("select e from ProviderUrlDB e where true").getResultList();
        if(resultList.isEmpty()){
            return contentProviderFactory.getDemoUrls().stream().map(e->new ProviderUrlDB(e)).map(e->em.merge(e)).toList();
        }
        return resultList;
    }
}
