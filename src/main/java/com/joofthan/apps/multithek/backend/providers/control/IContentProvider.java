package com.joofthan.apps.multithek.backend.providers.control;

import java.util.List;

public interface IContentProvider {

    List<Content> search(String text);
    List<Content> getContinueList();
    List<Content> getRecommendations();
    default ContentAction clickAction(UniversalContentId content){
        throw new UnsupportedOperationException();
    }

    ContentProviderInfo getInfo();

    default void doSubscribe(){
        throw new UnsupportedOperationException();
    }
    default void login(String user, String password){
        throw new UnsupportedOperationException();
    }
}
