package com.joofthan.apps.multithek.backend.providers.entity;

import jakarta.persistence.*;


@Deprecated
//@Entity
@Table(name = "CONTENTPROVIDER_DETAIL")
class ContentProviderKeyValueDetail {
    @Id
    @GeneratedValue
    private Long id;
    private String myKey;
    private String myValue;

    public ContentProviderKeyValueDetail() { }
    public ContentProviderKeyValueDetail(String myKey, String myValue) {
        this.myKey = myKey;
        this.myValue = myValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMyKey() {
        return myKey;
    }

    public void setMyKey(String key) {
        this.myKey = key;
    }

    public String getMyValue() {
        return myValue;
    }

    public void setMyValue(String value) {
        this.myValue = value;
    }
}
