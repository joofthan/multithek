package com.joofthan.apps.multithek.backend.multicontent.control;

import com.joofthan.apps.multithek.backend.providers.boundary.IContentProviderFactory;
import com.joofthan.apps.multithek.backend.providers.control.IContentProvider;
import com.joofthan.lib.InjectableProxy;
import jakarta.inject.Inject;

import java.util.ArrayList;
import java.util.List;

@InjectableProxy
public class MultiContentRepository {

    private List<IContentProvider> providers;

    @Inject
    public MultiContentRepository(IContentProviderFactory contentProviderFactory){
        providers = contentProviderFactory.getConfiguredProviders();
    }

    public MultiContentRepository(List<IContentProvider> providers) {
        this.providers = providers;
    }


    public List<MultiContent> getContinue(){
        ArrayList<MultiContent> multiContents = new ArrayList<>();
        for (int i = 0; i < providers.size(); i++) {
            var contentList = providers.get(i).getContinueList();
            long id = i;
            var providerContent = contentList.stream().map(e -> MultiContent.of(e, MultiContent.ContentProviderId.of(id))).toList();
            multiContents.addAll(providerContent);
        }
        return multiContents;
    }


    public List<MultiContent> getRecommendations(){
        ArrayList<MultiContent> multiContents = new ArrayList<>();
        for (int i = 0; i < providers.size(); i++) {
            var contentList = providers.get(i).getRecommendations();
            long id = i;
            var providerContent = contentList.stream().map(e -> MultiContent.of(e, MultiContent.ContentProviderId.of(id))).toList();
            multiContents.addAll(providerContent);
        }
        return multiContents;
    }

}
