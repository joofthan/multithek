package com.joofthan.apps.multithek;

import com.joofthan.lib.Injectable;
import java.time.LocalDateTime;

@Injectable
public class CurrentTime {
    public LocalDateTime get(){
        return LocalDateTime.now();
    }
}
