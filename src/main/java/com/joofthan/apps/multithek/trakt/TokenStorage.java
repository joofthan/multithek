package com.joofthan.apps.multithek.trakt;

import jakarta.enterprise.context.RequestScoped;
import java.util.Optional;

@RequestScoped
public class TokenStorage {

    private String token;
    private String state;
    private String backTo;

    public Optional<String> findToken() {
        return Optional.ofNullable(token);
    }

    public void saveToken(String token) {
        this.token = token;
    }

    public void saveState(String state) {
        this.state = state;
    }

    public Optional<String> findState() {
        return Optional.ofNullable(state);
    }

    public void saveBackTo(String backTo) {
        this.backTo=backTo;
    }

    public Optional<String> findBackTo() {
        return Optional.ofNullable(backTo);
    }
}
