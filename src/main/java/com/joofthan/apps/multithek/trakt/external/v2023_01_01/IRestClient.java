package com.joofthan.apps.multithek.trakt.external.v2023_01_01;

import java.util.List;

public interface IRestClient {
    void header(String key, String value);
    <T> List<T> getList(Class<T> type, String url);
    <T> String post(String url, T requestBody);
}
