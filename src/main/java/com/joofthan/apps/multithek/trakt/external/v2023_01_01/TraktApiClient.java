package com.joofthan.apps.multithek.trakt.external.v2023_01_01;


import com.joofthan.apps.multithek.trakt.external.v2023_01_01.IRestClient;

import java.util.List;

class TraktApiClient {
    private final String baseUrl;
    private final IRestClient restClient;
    private final String clientID;

    public TraktApiClient(String baseApiUrl, IRestClient res, String clientID, String accessToken){
        this.baseUrl = baseApiUrl;
        restClient = res;
        restClient.header("Authorization", "Bearer "+accessToken);
        restClient.header("trakt-api-key", clientID);
        restClient.header("trakt-api-version", "2");
        this.clientID = clientID;
        //String accesToken = "todo";
        //restClient.header("Authorization", "Bearer "+accesToken);
    }

    public List<String> getRecommendedMovies(){

        List<String> test = restClient.getList(String.class, baseUrl + "/movies/recommended");

        return test;

    }

    public String clientID() {
        return clientID;
    }
}
