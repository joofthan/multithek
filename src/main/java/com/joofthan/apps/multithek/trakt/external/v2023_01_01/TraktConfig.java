package com.joofthan.apps.multithek.trakt.external.v2023_01_01;


import com.joofthan.apps.multithek.trakt.TokenStorage;

public record TraktConfig(
        TokenStorage tokenStorage,
        String baseApiUrl,
        String baseAuthoriseUrl,
        String clientId,
        String clientSecret
) {}
