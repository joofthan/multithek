package com.joofthan.apps.multithek.trakt;

import com.joofthan.apps.multithek.trakt.external.v2023_01_01.TraktApi;


public class AuthoriseTrakt {
    private final TraktApi traktApi;

    public AuthoriseTrakt(TraktApi traktAuthoriseService) {
        this.traktApi = traktAuthoriseService;
    }


    public TraktAuthoriseResponse execute(TraktAuthoriseRequest req) {
        traktApi.authorise(req.code(),req.state());
        return new TraktAuthoriseResponse(traktApi.isAuthorised());
    }

    public static record TraktAuthoriseRequest(
            String state,
            String code
    ) {}

    public static record TraktAuthoriseResponse(
            boolean isAuthorised
    ) {}
}
