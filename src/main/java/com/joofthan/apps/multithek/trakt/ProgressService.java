package com.joofthan.apps.multithek.trakt;

import com.joofthan.apps.multithek.trakt.external.v2023_01_01.TraktApi;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;

@RequestScoped
public class ProgressService {
    @Inject
    TraktApi traktApi;


    public TraktLoadStatusResponse loadStatusAndSaveState(String backToUrl) {
        return new TraktLoadStatusResponse(traktApi.isAuthorised(), traktApi.authoriseUrl(backToUrl));
    }

    public void authorise(String code, String valueOf) {
        traktApi.authorise(code,valueOf);
    }

    public record TraktLoadStatusResponse(
            boolean isAuthorised,
            String authoriseUrl
    ){}
}
