package com.joofthan.apps.multithek.trakt.external.v2023_01_01;

import com.joofthan.apps.multithek.trakt.TokenStorage;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@RequestScoped
public class TraktApi {
    //@Inject TODO
    IRestClient restClient;
    @Inject
    TokenStorage traktUserRepository;
    @Inject
    RandomStateGenerator random;
    @Inject
    TraktConfig traktConfig;

    private TraktApiClient traktApi = null;


    public boolean isAuthorised(){
        return traktUserRepository.findToken().isPresent();
    }


    public String authoriseUrl(String backTo) {
        var state = String.valueOf(random.generate(0, Short.MAX_VALUE));
        traktUserRepository.saveState(state);
        traktUserRepository.saveBackTo(backTo);
        String url = traktConfig.baseAuthoriseUrl()+"/oauth/authorize?response_type=code&client_id=" +traktConfig.clientId() + "&redirect_uri="+ URLEncoder.encode(backTo, StandardCharsets.UTF_8)+"&state="+state;
        return url;
    }


    public void authorise(String codeFromTraktRedirect, String state) {
        var savedState = traktUserRepository.findState();
        if(savedState.orElseThrow(()->new RuntimeException("No saved State found.")).equals(state)){
            var requestBody = new GetTokenRequest(
                    codeFromTraktRedirect,
                    traktConfig.clientId(),
                    traktConfig.clientSecret(),
                    traktUserRepository.findBackTo().orElseThrow(()-> new RuntimeException("No backToUrl found.")),
                    "authorization_code"
            );

            var token = restClient.post(traktConfig.baseAuthoriseUrl()+"oauth/token", requestBody);
            traktUserRepository.saveToken(token);
        }else{
            throw new RuntimeException("Trakt authorise state does not match.");
        }
    }

    private TraktApiClient getApiClient(){
        if(traktApi == null){
            traktApi = new TraktApiClient(traktConfig.baseApiUrl(), restClient, traktConfig.clientId(), traktUserRepository.findToken().orElseThrow(()->new RuntimeException("Trakt is not Authorised.")));
        }
        return traktApi;
    }

    public record GetTokenRequest(
        String code,
        String client_id,
        String client_secret,
        String redirect_uri,
        String grant_type
    ){}
}
