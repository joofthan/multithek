package com.joofthan.apps.multithek.trakt.external.v2023_01_01;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;

@ApplicationScoped
public class TraktConfigProducer {

    //@ConfigProperty(name = "trakt.base.api.url")
    String baseApiUrl;

    @Produces
    public TraktConfig create(){
        return new TraktConfig(null,null,null,null,null);
    }
}
