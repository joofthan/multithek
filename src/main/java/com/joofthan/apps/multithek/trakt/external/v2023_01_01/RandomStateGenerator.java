package com.joofthan.apps.multithek.trakt.external.v2023_01_01;

import jakarta.enterprise.context.RequestScoped;

@RequestScoped
public class RandomStateGenerator {
    public int generate(int from, short to) {
        return (int) (from + (Math.random() * (to+1)));
    }
}
