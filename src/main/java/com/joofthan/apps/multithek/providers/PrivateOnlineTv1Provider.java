package com.joofthan.apps.multithek.providers;

import com.joofthan.apps.multithek.backend.providers.control.*;
import com.joofthan.apps.multithek.CurrentTime;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import java.util.List;

@RequestScoped
class PrivateOnlineTv1Provider implements IContentProvider {
    @Inject
    CurrentTime currentTime;

    Content proSieben = entry("ProSieben", 1);
    @Override
    public List<Content> search(String text) {
        return List.of();
    }

    @Override
    public List<Content> getContinueList() {
        if(currentTime.get().getHour() == 19){
            return List.of(proSieben);
        }
        return List.of();
    }

    @Override
    public List<Content> getRecommendations() {
        return List.of(
                entry("Das Erste", 165),
                entry("ZDF", 123),
                proSieben,
                entry("Sat1", 2),
                entry("Kabel1", 3),
                entry("Comedy Central", 130),
                entry("MTV", 129),
                entry("DeluxeMusic", 127),
                entry("DMAX", 110),
                entry("Nick", 131),
                entry("SWR",152 ),
                entry("ZDF Neo", 125),
                entry("One", 168),
                entry("ARD Alpha", 169),
                entry("3sat", 1006),
                entry("Pro7Maxx", 5),
                entry("sixx", 4),
                entry("Sport1", 115),
                entry("Eurosport1", 122)
        );
    }

    @Override
    public ContentProviderInfo getInfo() {
        return new ContentProviderInfo(
                false,
                SubscriptionPrice.none(),
                "joyn.de",
                "Joyn Tv Provider",
                List.of(),
                List.of()
        );
    }
    long currentId=1L;
    private Content entry(String title, int channelId){
        return new Content(
                UniversalContentId.of(currentId++),
                Content.ContentType.LIVE,
                title,
                title + " Description",
                Image.NONE,
                null,
                0,
                ContentAction.url("https://www.joyn.de/play/live-tv?channel_id="+channelId)
        );
    }
}
