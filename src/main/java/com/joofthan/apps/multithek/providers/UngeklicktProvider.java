package com.joofthan.apps.multithek.providers;

import com.joofthan.apps.multithek.backend.providers.control.*;
import com.joofthan.apps.multithek.CurrentTime;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import java.util.List;

@RequestScoped
class UngeklicktProvider implements IContentProvider {

    @Inject
    CurrentTime currentTime;

    @Override
    public List<Content> search(String text) {
        return List.of();
    }

    @Override
    public List<Content> getContinueList() {
        return getIfAvailable();
    }

    private List<Content> getIfAvailable() {
        var time = currentTime.get().toLocalTime();
        if(time.getHour() == 20){
            return List.of(
                    new Content(
                            UniversalContentId.of(1L),
                            Content.ContentType.LIVE,
                            "Ungeklickt",
                            "Ungeklickt",
                            Image.NONE,
                            currentTime.get(),
                            0,
                            ContentAction.url("https://unge.tv/")
                    )
            );
        }
        return List.of();
    }

    @Override
    public List<Content> getRecommendations() {
        return getIfAvailable();
    }

    @Override
    public ContentProviderInfo getInfo() {
        return new ContentProviderInfo(
                true,
                SubscriptionPrice.none(),
                "unge.tv",
                "Ungeklickt CDI Provider always between 8pm and 9pm",
                List.of(),
                List.of()
        );
    }

}
