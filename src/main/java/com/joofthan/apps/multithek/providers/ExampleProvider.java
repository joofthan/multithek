package com.joofthan.apps.multithek.providers;


import com.joofthan.apps.multithek.backend.providers.control.*;

import jakarta.enterprise.context.RequestScoped;
import java.time.LocalDateTime;
import java.util.List;

@RequestScoped
class ExampleProvider implements IContentProvider {

    @Override
    public List<Content> search(String text) {
        return List.of();
    }

    @Override
    public List<Content> getContinueList() {
        return List.of(
                new Content(
                UniversalContentId.of(1L),
                Content.ContentType.MOVIE,
                "Title",
                "descr",
                "https://m.media-amazon.com/images/M/MV5BZWJhYjFmZDEtNTVlYy00NGExLWJhZWItNTAxODY5YTc3MDFmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg",
                LocalDateTime.now(),
                10,
                ContentAction.url("http://details")//TODO: Can this know the action url of Multithek?
                )
                ,
                new Content(
                        UniversalContentId.of(1L),
                        Content.ContentType.MOVIE,
                        "Title",
                        "descr",
                        "https://m.media-amazon.com/images/M/MV5BZWJhYjFmZDEtNTVlYy00NGExLWJhZWItNTAxODY5YTc3MDFmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg",
                        LocalDateTime.now(),
                        10,
                        ContentAction.url("http://details")//TODO: Can this know the action url of Multithek?
                ),
                new Content(
                UniversalContentId.of(1L),
                Content.ContentType.MOVIE,
                "Title",
                "descr",
                "https://m.media-amazon.com/images/M/MV5BZWJhYjFmZDEtNTVlYy00NGExLWJhZWItNTAxODY5YTc3MDFmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg",
                LocalDateTime.now(),
                10,
                ContentAction.url("http://details")//TODO: Can this know the action url of Multithek?
        )
        );
    }

    @Override
    public List<Content> getRecommendations() {
        return List.of();
    }

    @Override
    public ContentAction clickAction(UniversalContentId content) {
        return ContentAction.url("http://test");
    }

    @Override
    public ContentProviderInfo getInfo() {
        return new ContentProviderInfo(
                false,
                SubscriptionPrice.month(9999),
                "example",
                "test contetn provider",
                List.of(),
                List.of()
        );
    }

}
