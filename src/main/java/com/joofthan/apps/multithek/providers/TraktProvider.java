package com.joofthan.apps.multithek.providers;

import com.joofthan.apps.multithek.backend.providers.control.Content;
import com.joofthan.apps.multithek.backend.providers.control.ContentProviderInfo;
import com.joofthan.apps.multithek.backend.providers.control.IContentProvider;
import com.joofthan.apps.multithek.backend.providers.control.SubscriptionPrice;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Vetoed;
import java.util.List;

@Vetoed
@RequestScoped
public class TraktProvider implements IContentProvider {

    @Override
    public List<Content> search(String text) {
        return null;
    }

    @Override
    public List<Content> getContinueList() {

        return null;
    }

    @Override
    public List<Content> getRecommendations() {
        return null;
    }

    @Override
    public ContentProviderInfo getInfo() {
        return new ContentProviderInfo(
                false,
                SubscriptionPrice.none(),
                "trakt.tv",
                "Trakt Dummy Provider",
                List.of(),
                List.of()
        );
    }
}
