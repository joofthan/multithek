package com.joofthan.apps.multithek.frontend;

import com.joofthan.lib.TransactionalBoundary;
import com.joofthan.lib.template.Template;
import com.joofthan.apps.multithek.MultithekApi;
import com.joofthan.apps.multithek.backend.home.control.HomeService;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@TransactionalBoundary
@Path(MultithekApi.PATH)
public class HomeHtml {

    @Inject
    HomeService home;

    @Inject
    Template template;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String main(){
        //language="html"
        return template.render("""
            <!DOCTYPE html>
            <html>
            <head>
              <title>Multithek</title>
              <link rel="stylesheet" href="/multithek/css/style.css" />
              <link rel="stylesheet" href="/shared/apputil/autostyle.css" />
              <script src="/webjars/htmx.org/dist/htmx.min.js"></script>
            </head>
            <body style="margin: 0;padding: 0;">
            <div id='messages'></div>
            <div class="app-main">
              <div class="sidebar">
                <h2 class="logo svelte-j9zhma">Multithek</h2>
                <div class="nav" hx-get="/multithek/html/menue" hx-trigger="load">
            
                </div>
              </div>
              <div class="header">
                <input id="search" type="text" placeholder="Suchen" />
              </div>
              <div class="center"><br /><br /><br />
            
              </div>
              <main id="content" hx-get="/multithek/html/home" hx-trigger="load">
            
              </main>
            </div>
            </body>
            </html>
                    """);
    }

    @GET
    @Path("html/home")
    @Produces(MediaType.TEXT_HTML)
    public String homeContent(){
        return template.render("""
                    <div class="row " id="upnext">
                    <h4 class="row-head">Continue</h4>
                    {#each covers}
                         <a href="{it.url}" class="cover">
                            <img src="{it.image}" alt="{it.placeholder}"/>
                         </a>
                     {/each}
                    ""","covers", home.homeContinue());
    }

    @GET
    @Path("html/home/test2")
    @Produces(MediaType.TEXT_HTML)
    public String thymeleaf(){
        return template.render("""
                 <a th:each="c : ${covers}" th:href="${c.url}" class="cover">
                    <img th:src="${c.image}" th:alt="${c.placeholder}"/>
                 </a>
                ""","covers",home.homeContinue());
    }

    @GET
    @Path("html/home/my")
    @Produces(MediaType.TEXT_HTML)
    public String my(){
        return template.render("""
                 <a foreach="covers as c" href="{c.url}" class="cover">
                    <img src="{c.image}" alt="{c.placeholder}"/>
                 </a>
                ""","covers",home.homeContinue());
    }

    public String full(){
        return template.render("""
                 <a foreach="covers as c" href="{c.url}" class="cover">
                    <img if="covers.size() >= 5" src="{c.image}" alt="{c.placeholder}"/>
                    <img else if="xxx" src="{c.image}" alt="{c.placeholder}"/>
                    <img else src="{c.image}" alt="{c.placeholder}"/>
                 </a>
                ""","covers",home.homeContinue());
    }
}
