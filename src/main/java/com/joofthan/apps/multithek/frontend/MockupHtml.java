package com.joofthan.apps.multithek.frontend;

import com.joofthan.apps.multithek.MultithekApi;

import com.joofthan.lib.TransactionalBoundary;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@TransactionalBoundary
@Path(MultithekApi.PATH+"/api/mockup")
public class MockupHtml {
    @GET
    public String mockup(){
        return """
               <img src="img/mockup.png"></img>
               """;
    }
}
