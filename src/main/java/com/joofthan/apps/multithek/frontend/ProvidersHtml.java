package com.joofthan.apps.multithek.frontend;

import com.joofthan.apps.multithek.MultithekApi;

import com.joofthan.lib.TransactionalBoundary;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@TransactionalBoundary
@Path(MultithekApi.PATH+"/api/providers")
public class ProvidersHtml {
    @GET
    public String providers(){
        return """
                <div style="text-align: center;font-size: 1.5em;">
                    <h3>Free</h3>
                    <a class="link" href="https://www.ardmediathek.de" target="_blank">ARD Mediathek</a><br/>
                    <a class="link" href="https://www.zdf.de" target="_blank">ZDF Mediathek</a><br/>
                    <a class="link" href="https://www.netzkino.de/" target="_blank">Netzkino</a><br/>
                    <a class="link" href="https://www.tvnow.de/specials/watchbox" target="_blank">Watchbox</a><br/>
                    <a class="link" href="https://www.joyn.de/" target="_blank">Joyn</a><br/>
                    <div class="row">
                        <a class="link" href="https://www.youtube.de" target="_blank">YouTube</a>(<a class="link" href="https://www.youtube.com/feed/subscriptions" target="_blank">Abos</a>/<a class="link" href="https://www.youtube.com/playlist?list=WL" target="_blank">Später ansehen</a>)<br/>
                    </div>
        
                    <h3>More</h3>
                    <a class="link" href="https://www.amazon.de/Amazon-Video/b/?ie=UTF8&amp;node=3010075031" target="_blank">Amazon Prime Video</a><br/>
                    <a class="link" href="https://www.disneyplus.com/de-de/" target="_blank">Disney+</a><br/>
                    <a class="link" href="https://www.netflix.com/de/" target="_blank">Netflix</a><br/>
                    <a class="link" href="https://skyticket.sky.de/watch/home" target="_blank">Sky Ticket</a><br/>
                    <a class="link" href="https://www.tvnow.de/" target="_blank">Tv Now</a><br/>
                    <h3>A-Z</h3>
                    <a class="link" href="https://www.amazon.de/Amazon-Video/b/?ie=UTF8&amp;node=3010075031" target="_blank">Amazon Prime Video</a><br/>
                    <a class="link" href="https://www.ardmediathek.de" target="_blank">ARD Mediathek</a><br/>
                    <a class="link" href="https://www.disneyplus.com/de-de/" target="_blank">Disney+</a><br/>
                    <a class="link" href="https://www.joyn.de/" target="_blank">Joyn</a><br/>
                    <a class="link" href="https://www.netflix.com/de/" target="_blank">Netflix</a><br/>
                    <a class="link" href="https://www.netzkino.de/" target="_blank">Netzkino</a><br/>
                    <a class="link" href="https://skyticket.sky.de/watch/home" target="_blank">Sky Ticket</a><br/>
                    <a class="link" href="https://www.tvnow.de/" target="_blank">Tv Now</a><br/>
                    <a class="link" href="https://www.tvnow.de/specials/watchbox" target="_blank">Watchbox</a><br/>
                    <a class="link" href="https://www.youtube.de" target="_blank">YouTube</a><br/>
                    <a class="link" href="https://www.zdf.de" target="_blank">ZDF Mediathek</a><br/>
                </div>
                """;
    }
}
