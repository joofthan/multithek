package com.joofthan.apps.multithek.frontend;

import com.joofthan.apps.multithek.backend.providers.boundary.IContentProviderFactory;
import com.joofthan.lib.TransactionalBoundary;
import com.joofthan.lib.template.Template;
import com.joofthan.apps.multithek.backend.home.boundary.SettingsEndpoint;
import com.joofthan.apps.multithek.backend.home.entity.Link;
import com.joofthan.apps.multithek.MultithekApi;
import lombok.RequiredArgsConstructor;

import jakarta.ws.rs.*;
import java.util.Map;

@TransactionalBoundary
@RequiredArgsConstructor
@Path(MultithekApi.PATH +"/settings/html")
public class SettingsHtml {

    final SettingsEndpoint settingsEndpoint;
    final Template template;
    final IContentProviderFactory contentProviderFactory;


    @GET
    public String providersContent(){
        //language="html"
        return template.render("""
                    {#each providers}
                        <a class='link' href="#" hx-get="{it.url}" hx-target="#content" hx-trigger="click">{it.url}</a>
                        <a class='link' style='color: red' href="#" hx-delete="{url}/{it.id}" hx-target="#content" hx-trigger="click">X</a>
                        <br>
                    {/each}
                    <br>
                    <form id='addurl'>
                        <input class='autostyle' type='text' name='url'>
                        <button class='autostyle' hx-post='{url}' hx-trigger='click' hx-inclue='#addurl' hx-target='#content'>Add</button>
                    </form>      
                    <br>
                    Available Demo Urls: {demoUrls}
""", Map.of(
                "providers",settingsEndpoint.listProviders(),
                "url", Link.to(SettingsHtml.class),
                "demoUrls",contentProviderFactory.getDemoUrls()
        ));
    }

    @POST
    public String add(@FormParam("url") String url){
        settingsEndpoint.add(url);
        return providersContent();
    }

    @DELETE
    @Path("{id}")
    public String delete(@PathParam("id") Long id){
       settingsEndpoint.delete(id);
       return providersContent();
    }
}
