package com.joofthan.apps.multithek.frontend;

import com.joofthan.lib.TransactionalBoundary;
import com.joofthan.lib.template.Template;
import com.joofthan.apps.multithek.backend.home.boundary.LiveEndpoint;
import com.joofthan.apps.multithek.backend.home.entity.Link;
import com.joofthan.apps.multithek.backend.home.entity.MenueItem;
import com.joofthan.apps.multithek.MultithekApi;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.io.Serializable;
import java.util.List;

@TransactionalBoundary
@Path(MultithekApi.PATH)
public class MenueHtml implements Serializable {

    @Inject
    Template template;

    List<MenueItem> entries = List.of(
            new MenueItem("Home", Link.to(HomeHtml.class, "/html/home")),
            new MenueItem("Live TV", Link.to(LiveEndpoint.class)),
            new MenueItem("Providers", Link.to(ProvidersHtml.class)),
            new MenueItem("Mockup", Link.to(MockupHtml.class)),
            new MenueItem("Planning", Link.to(PlanHtml.class)),
            new MenueItem("Settings", Link.to(SettingsHtml.class))
    );

    @GET
    @Path("html/menue")
    public String menue() {
        return template.render("""
                    {#each entries}
                        <a href="#" hx-get="{it.target.url}" hx-target="#content" hx-trigger="click">{it.text}</a>
                    {/each}
                    <a href="/login/logout">Logout</a>
                ""","entries",entries);
    }
}
