package com.joofthan.apps.login.boundary.html;

import com.joofthan.lib.template.Template;

import jakarta.inject.Inject;
import jakarta.ws.rs.CookieParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.NewCookie;
import jakarta.ws.rs.core.Response;


@Produces(MediaType.TEXT_HTML)
@Path("login")
public class MainHtml {
    public static String LOGIN_BUTTON = "<a href=\"/login/\">Login</a>";

    @Inject
    Template template;


    @GET
    public String get(){
        //language="html"
        return """
                <!DOCTYPE html>
                <html >
                <head>
                    <title>Login</title>
                </head>
                <body>
                <script type="module">
                    import {el, onClick, post} from "../shared/apputil/apputil.js";
                                
                    let backTo = document.referrer;
                    onClick("#login", login);
                    onClick("#register", ()=>{
                        let user = el("#user").value;
                        let pw = el("#pw").value;
                        post("/login/api/register", {'user':user,'password':pw}).then((res)=>{
                            alert("registered");
                        });
                    });
                    addEventListener("keydown", e=>{
                    if(e.code === "Enter"){
                        login();
                    }
                    });
                    
                    function login() {
                       let user = el("#user").value;
                       let pw = el("#pw").value;
                       post("/login/api/token", {'username':user,'password':pw}).then((res)=>{
                           location.href=backTo;
                       });
                    }
                </script>
                        Username: <input id="user" name="username"><br/>
                        Password: <input id="pw" type="password" name="pw"/><br/>
                        <button id="login" type="submit">Login</button>
                        <button id="register">Register</button>
                </body>
                </html>
                """;
    }

}
