package com.joofthan.apps.login.adapter.hasher;

import io.quarkus.elytron.security.common.BcryptUtil;

import jakarta.enterprise.context.RequestScoped;

@RequestScoped
public class PasswordHasher {

    public String hash(String password){
        return BcryptUtil.bcryptHash(password);
    }

    public boolean isValid(String password, String hash){
        return BcryptUtil.matches(password, hash);
    }
}
